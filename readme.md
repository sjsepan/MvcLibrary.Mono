# readme.md - README for MvcLibrary.Mono 0.3

## About

App-specific library of application functions and classes for C# Mono applications; requires Ssepan.Application.Mono, ssepan.io.mono, ssepan.utility.mono

### Purpose

To serve as a reference architecture and template for console and winforms apps that share a common model and settings file.
Also demonstrates my interpretation of the Model-View-Controller pattern.

### Usage notes

~N/A

### History

0.3:
~Initial release for Mono, cloned from *.Core project

### Fixes

### Instructions for downloading/installing Newtonsoft.Json

JSON serialization is used by Ssepan.Application.Mono, but is not available in System.Text[.Json.Serialization]. To get JSON working, install from package manager:
<https://www.codeproject.com/Questions/747284/Json-NET-doesnt-work-with-Mono-under-Debian>
<https://stackoverflow.com/questions/36550910/what-is-the-json-net-mono-assembly-reference>
sudo apt-get install Newtonsoft.Json

### Known Issues

~I have observed that having 'using' statements where one is contained within the other causes the inner namespace to not be found and the compile to fail. For example, with 'Ssepan.Application.Mono' and 'Ssepan.Ui.Mono.Terminal', the latter cannot be resolved. If fact, I encountered several situations in this set of projects where compile errors were resolved by commenting-out severalin 'using' statements that I would otherwise have kept in .Net. The solution is to comment it out and rely on the former alone. This may be a behavior of the xbuild tool. Possible related issues:
<https://bugzilla.xamarin.com/12/12165/bug.html>
<https://bugs.launchpad.net/ubuntu/+source/mono/+bug/1875850>
<https://social.msdn.microsoft.com/Forums/vstudio/en-US/71bcb26b-3149-41c7-9ad7-fe908ffe983e/msbuild-on-linux?forum=msbuild>
<https://peaku.co/questions/16791-%C2%BFcomo-agregar-una-referencia-de-ensamblaje-faltante-para-xbuild-(mono-linux>)

Steve Sepan
ssepanus@yahoo.com
5/31/2022
